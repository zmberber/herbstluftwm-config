#!/usr/bin/env bash

# This turns fullscreen off if focus is changed to a different client on the same tag with the focus command
DIR=$1
WINID=$(herbstclient attr clients.focus.winid)
herbstclient focus $DIR
if [ $(echo $?) = 0 ] ; then
    herbstclient set_attr clients.$WINID.fullscreen false
fi
